package com.mitocode.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.mindrot.jbcrypt.BCrypt;

import com.mitocode.model.Usuario;
import com.mitocode.service.IUsuarioService;

@Named
@ViewScoped
public class UsuarioBean implements Serializable {

	@Inject
	private IUsuarioService usuarioService;
	private Usuario usuario;
	private List<Usuario> listaUs;
	private String nombreUsuario;
	private String passActual;
	private String passNuevo;
	private String passRepit;
	public String getPassRepit() {
		return passRepit;
	}

	public void setPassRepit(String passRepit) {
		this.passRepit = passRepit;
	}

	private boolean disable;

	@PostConstruct
	public void init() {
		this.usuario = new Usuario();
		this.listaUs = new ArrayList<>();
		this.disable= true;
	}

	public boolean isDisable() {
		return disable;
	}

	public void setDisable(boolean disable) {
		this.disable = disable;
	}

	public String getPassActual() {
		return passActual;
	}

	public void setPassActual(String passActual) {
		this.passActual = passActual;
	}

	public String getPassNuevo() {
		return passNuevo;
	}

	public void setPassNuevo(String passNuevo) {
		this.passNuevo = passNuevo;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public String getNombreUsuario() {
		return nombreUsuario;
	}

	public void setNombreUsuario(String nombreUsuario) {
		this.nombreUsuario = nombreUsuario;
	}

	public List<Usuario> getListaUs() {
		return listaUs;
	}

	public void setListaUs(List<Usuario> listaUs) {
		this.listaUs = listaUs;
	}

	public void buscarUsuario() {
		try {
			this.listaUs = this.usuarioService.buscarUsuario(nombreUsuario);
			
			if (this.listaUs.isEmpty())
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,"No se encontraron coincidencias para: "+ nombreUsuario, "'" + nombreUsuario + "'"));
		} catch (Exception e) {

		}
	}
	
	public void cargarDataUsuario(Usuario u){
		this.usuario = u;
	}
	
	public void  verificarPassword(){
		try {
			String passHash = this.usuario.getContrasena();
			
			if(BCrypt.checkpw(this.passActual, passHash)) {
				this.disable = false;
			}else {
				this.disable = true;
			}		
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
	
	public void deshabilitar(){
		this.disable = true;
	}
	
	public String actualizarPassword()
	{
		String respuesta="";
		try {
			String clave = this.passNuevo;
			String claveHash = BCrypt.hashpw(clave, BCrypt.gensalt());
			this.usuario.setContrasena(claveHash);
			this.usuarioService.modificar(this.usuario);
			respuesta = "Se actualizo la contraseņa, correctamente";
		} catch (Exception e) {
			
		}
		
		return respuesta;
	}
}
